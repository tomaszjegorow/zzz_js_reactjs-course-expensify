import React from 'react';
import {shallow} from 'enzyme';
import {ExpensesSummary} from '../../components/ExpensesSummary';

test('should correctly render ExpensesSummary with single expense', () => {
    const wrapper = shallow(<ExpensesSummary expenseCount={1} expensesTotal={12312}/>);
    expect(wrapper).toMatchSnapshot();
});

test('should correctly ExpensesSummary with multiple expenses', () => {
    const wrapper = shallow(<ExpensesSummary expenseCount={23} expensesTotal={222123746}/>);
    expect(wrapper).toMatchSnapshot();
});
