const promise = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve({
            name: 'Tomasz',
            age: 33
        });

        // reject('Something went wrong!');
    }, 5000);
});

console.log('before');

promise
    .then((data) => {
        console.log(data);

        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve('This is my other promise');
            }, 5000);
        });
    })
    .then((str) => {
        console.log('does this run? ', str); // The answer is: YES!
    })
    .catch((data) => {
        console.log('error: ', data);
    });

console.log('after');

// const promise = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         resolve({
//             name: 'Tomasz',
//             age: 33
//         });
//
//         // reject('Something went wrong!');
//     }, 5000);
// });
//
// console.log('before');
//
// promise
//     .then((data) => {
//         console.log(data);
//
//         return 'some data';
//     })
//     .then((str) => {
//         console.log('does this run? ', str); // The answer is: YES!
//     })
//     .catch((data) => {
//         console.log('error: ', data);
//     });
//
// console.log('after');